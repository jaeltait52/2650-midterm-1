import React, { Component } from 'react';

class Cars extends Component {

  deleteCar(id, ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { carService } = this.props;

    /////////////////////////////////////////////////
    // STEP 15
    // WRITE THE CODE HERE TO REMOVE THE SELECTED CAR
    // START v
    /////////////////////////////////////////////////

  


    /////////////////////////////////////////////////
    // END ^
    /////////////////////////////////////////////////
  }
  
  addCar(ev) {
    // HERE IS YOUR HANDLE TO THE BACKEND CAR SERVICE
    const { carService } = this.props;
    
    ///////////////////////////////////////
    // STEP 12
    // WRITE THE CODE HERE TO ADD A NEW CAR
    // START v
    ///////////////////////////////////////
  
    const inputModel = ev.target.querySelector('[id="model"]');
    const inputMake = ev.target.querySelector('id="make"');
    const inputYear = ev.target.querySelector('[id="year"]');
    const inputMileage = ev.target.querySelector('[id="mileage"]');
    const make = inputMake.value.trim();
    const model = inputModel.value.trim();
    const year = parseInt(inputYear.value.trim());
    const mileage = parseInt(inputMileage.value.trim());
    


    this.state.items.create({
      make,
      model,
      year,
      mileage
    })
    .then(() => {
      inputModel.value = '';
      inputMake.value = '';
      inputYear.value = '2020';
      inputMileage.value = '0';
    });


    ///////////////////////////////////////
    // END ^
    ///////////////////////////////////////
    
    ev.preventDefault();
  }

  render() {
    const { cars } = this.props;

    return(
    <div>
      <div className="py-5 text-center">
        <h2>Cars</h2>
      </div>

      <div className="row">
        <div className="col-md-12 order-md-1">
          <form onSubmit={this.addCar.bind(this)} className="needs-validation" noValidate>
            <div className="row">
              <div className="col-md-4 mb-3">
                <label htmlFor="make">Make</label>
                <input type="text" className="form-control" id="make" defaultValue="" required />
                <div className="invalid-feedback">
                    A car make is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="model">Model</label>
                <input type="text" className="form-control" id="model" defaultValue="" required />
                <div className="invalid-feedback">
                    A car model is required.
                </div>
              </div>

              <div className="col-md-4 mb-3">
                <label htmlFor="year">Year</label>
                <input type="number" className="form-control" id="year" defaultValue="2020" required />
                <div className="invalid-feedback">
                    A model year is required.
                </div>
              </div>
              
              <div className="col-md-4 mb-3">
                <label htmlFor="mileage">Mileage</label>
                <input type="number" className="form-control" id="mileage" defaultValue="0" required />
                <div className="invalid-feedback">
                    A mileage is required.
                </div>
              </div>

            </div>
            <button className="btn btn-primary btn-lg btn-block" type="submit">Add car</button>
          </form>
        </div>
      </div>
      
      <table className="table">
        <thead>
          <tr>
            <th scope="col">ID</th>
            <th scope="col">Make</th>
            <th scope="col">Model</th>
            <th scope="col">Year</th>
            <th scope="col">Delete</th>
          </tr>
        </thead>
        <tbody>

          {cars && cars.map(car => <tr key={car.id}>
            <th scope="row">{car.id}</th>
            <td>{car.make}</td>
            <td>{car.model}</td>
            <td>{car.year}</td>
            <td><button onClick={this.deleteCar.bind(this, car.id)} type="button" className="btn btn-danger">Delete</button></td>
          </tr>)}

        </tbody>
      </table>

      <footer className="my-5 pt-5 text-muted text-center text-small">
        <p className="mb-1">&copy; 2020 CPSC 2650</p>
      </footer>
    </div>
    );
  }
}

export default Cars;
