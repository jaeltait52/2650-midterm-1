
import modelCheck from '../../hooks/model-check';
import yearCheck from '../../hooks/year-check';
import yearCheck from '../../hooks/year-check';
import carCheck from '../../hooks/car-check';
export default {
  before: {
    all: [],
    find: [],
    get: [],
    create: [modelCheck(), yearCheck(), yearCheck(), carCheck()],
    update: [],
    patch: [],
    remove: []
  },

  after: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  },

  error: {
    all: [],
    find: [],
    get: [],
    create: [],
    update: [],
    patch: [],
    remove: []
  }
};
