// Use this hook to manipulate incoming or outgoing data.
// For more information on hooks see: http://docs.feathersjs.com/api/hooks.html
import { Hook, HookContext } from '@feathersjs/feathers';

export default (options = {}): Hook => {
  return async (context: HookContext) => {
    
    
    const { data } = context;
    
    
    if(!data.text) {
      throw new Error('All forms must be filled');
    }
    
    const year = context.params.year;
    const mileage = context.params.mileage;
    
    if(year < 1885 || year > 2020){
      throw new Error('Year must be between 1885 and 2020');
    }
    
    if(mileage < 0){
      throw new Error("Milage cannot be negative");
    }
    
    const model = context.params.model.substring(0,30);
    const make = context.params.make.substring(0,30);
    
    context.data = {
      make,
      model,
      year,
      mileage
    }
    
    return context;
  };
}
